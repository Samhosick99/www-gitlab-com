features:
  secondary:
    - name: "Leverage policies to remove Docker images"
      available_in: [core, starter, premium, ultimate]
      image_url: '/images/unreleased/docker_expiration_policy2.png'
      documentation_link: 'https://docs.gitlab.com/ee/user/packages/container_registry/#expiration-policy'
      reporter: trizzi
      stage: package
      categories:
        - 'container_registry'
      issue_url: 'https://gitlab.com/gitlab-org/gitlab/issues/15398'
      description: |
         Users are able to build a lot of Docker images as part of their pipelines, however, many of these images are only needed for a short period of time and can then be deleted. Up until now, there hasn't been an efficient way for developers to delete these images. This has resulted in either ballooning storage costs or system administrators intervening and manually removing images via the [Container Registry API](https://docs.gitlab.com/ee/api/container_registry.html), something that is error-prone and inefficient.
        
         In 12.7, we are excited to introduce Docker tag expiration policies for all new projects! This new feature will allow users of the GitLab Container Registry to identify a tag by name, select how many versions of the tag to keep, and define when it should be removed. For example, you can set up a policy that will remove all tag names that match a regex like the Git SHA, always keep at least 5 images, and remove any image that is older than 30 days.

