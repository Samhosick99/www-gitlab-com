---
layout: markdown_page
title: "Category Direction - Merge Trains"
---

- TOC
{:toc}

## Merge Trains

Keeping master green and ensuring the stability of collaboration on branches is vitally important. GitLab has introduced Merge Trains as an important way to accomplish this.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AMerge%20Trains)
- [Overall Vision](/direction/release)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/594)
- [Documentation](https://docs.gitlab.com/ee/ci/merge_request_pipelines/pipelines_for_merged_results/merge_trains/) 


### Overall Prioritization

TBD

## What's Next & Why
 
TBD

## Maturity Plan

This category is currently at the "Viable" maturity level, and our next maturity target is Complete ([see our definitions of maturity levels](https://about.gitlab.com/direction/maturity/)).

We currently have basic capabilities and want to continue and extend these in the upcoming releases.

Key deliverables to achieve this are:

- [Merge Trains should support fast forward merge](https://gitlab.com/gitlab-org/gitlab/issues/35628)
- [API support for merge trains](https://gitlab.com/gitlab-org/gitlab/issues/32665)
- [Add Merge Train support to AutoDevOps](https://gitlab.com/gitlab-org/gitlab/issues/121933)
- [Add/Remove TODO tasks according to the Merge Train events](https://gitlab.com/gitlab-org/gitlab/issues/12136)
- [Have merge quick action add to merge train](https://gitlab.com/gitlab-org/gitlab/issues/32336)

## Competitive Landscape

TBD

## Top Customer Issue(s) and Top Customer Success/Sales Issue(s)

TBD

## Top Internal Customer Issue(s)

TBD

## Top Vision Item(s)

TBD
