---
layout: markdown_page
title: "Kevin Chu's README"
---

## Kevin's README

Hello! I am Kevin and I'm the [Group Product Manager](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/job-families/product/group-manager-product/index.html.md) for the Monitor stage. This README is meant to tell you a bit more about myself and to provide a glimpse to what it might be like to work with me. Please feel free to contribute to this page by opening a merge request.

* [GitLab Handle](https://gitlab.com/kbychu)
* [Team Page](/company/team/#kbychu)

## About me

* I grew up in Taiwan and moved to Seattle, WA, USA when I was 10 with my family. I spent half of my 20s living in London and Madrid and loved it!
* I live in Portland, OR with my wife, 3 little kids, and a [dog](company/team-pets/#279-chopito).
* I love to laugh.
* I am excitable and wear my heart on my sleeve.
* I can struggle with starting too many things at once - I'd like to learn to do only one thing at a time.
* I am open to having my mind changed but otherwise can come across as being opinionated.
* I love GitLab's values. Particularly [iteration](https://about.gitlab.com/handbook/values/#iteration) and [transparency](https://about.gitlab.com/handbook/values/#transparency).
* I can recall random details or facts from past interactions.

## Communicating and working with me

* I value making decisions quickly. I believe having a decision is always better than having no decision because we can always iterate or reverse for two-way door decisions.
* I believe is is ok to figure things out along the way.
* I strive to be a good listener, but may often start reacting too quickly. Please feel free to call me out on it.
* I use design thinking in most things I do.
* I prefer direct feedback.
* I am quick to apologize and own mistakes when they happen.
* Team harmony is extremely important to me. I am committed to help create trusting and productive teams.
* I am not good at providing negative feedback and will need help to get better at it.
* I enjoy building deep, personal relationships with the people I work with. If this is not your preferred style of interaction, please feel free to let me know.